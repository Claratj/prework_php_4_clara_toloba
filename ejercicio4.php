<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <?php

        function reemplazar ($text_org, $text_mod, $word_org, $wordmod) {
            $line = file_get_contents ($text_org);
            $line_new = str_replace( $word_org, $wordmod, $line);
            file_put_contents($text_mod, $line_new);
        }
        reemplazar('el_quijote.txt', 'quijote-modificado.txt', 'Sancho', 'Morty');        
    ?>

</body>
</html>